require([
    "jquery",
    'mage/translate',
], function ($, $t) {
    $(function () {
        window.wkdailydealLoaded = false;
        var dataloaded = false;
        var updatedPro = [];
        $.ajax({
            type: "POST",
            url: window.BASE_URL+"mpdailydeal/index/gettimes",
            dataType: "json",
            cache: false,
            success: function (response) {
                if (response.success) {
                    dataloaded = true;
                    var data = response.data;
                    $.each($('.deal.wk-daily-deal'), function (ind, val) {
                        var dealId = $(val).data('deal-id');
                        if (dealId != undefined && dealId) {
                            if (data[dealId]!=undefined) {
                                updatedPro.push(dealId);
                                updatedPro = updatedPro.concat(data[dealId]['parent']);
                                var countClock = $(val).find('.wk_cat_count_clock');
                                $(countClock).attr('data-stoptime', data[dealId]['stoptime']);
                                $(countClock).attr('data-diff-timestamp', data[dealId]['diff_timestamp']);
                            } else {
                                // console.log("dealId");
                                // console.log(dealId);
                                dataloaded = false;
                            }
                        }
                    });
                    $.each($('div.price-box'), function (ind1, val1) {
                        var productId = $(val1).data('product-id');
                        if (productId != undefined && productId) {
                            if (data[productId]!=undefined && !(updatedPro.includes(productId) || updatedPro.includes(productId+''))) {
                                // console.log("productId");
                                // console.log(productId);
                                dataloaded = false;
                            }
                        }
                    });
                    if (dataloaded) {
                        $(document).ready(function () {
                            $('body').trigger('wkdailydealLoaded');
                            window.wkdailydealLoaded = true;
                        });
                    } else {
                        $.ajax({
                            type: "get",
                            url: window.BASE_URL+"mpdailydeal/index/cacheflush",
                            dataType: "json",
                            cache: false,
                            success: function (response) {
                                location.reload();
                            }
                        });
                    }
                }
            },
            error: function (response) {
            }
        });
    });
});

<?php
namespace Webkul\MpDailyDeal\Helper;

/**
 * Webkul_MpDailyDeal data helper
 * @category  Webkul
 * @package   Webkul_MpDailyDeal
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

use Magento\Framework\Stdlib\DateTime\TimezoneInterface;
use Magento\Framework\Pricing\Helper\Data as PricingHelper;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Downloadable\Api\LinkRepositoryInterface;
use Webkul\Marketplace\Model\ProductFactory;
use Magento\ConfigurableProduct\Model\Product\Type\Configurable as ConfigurableProTypeModel;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory as ProductCollectionFactory;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    /**
     * @var Magento\Framework\Stdlib\DateTime\TimezoneInterface
     */
    public $localeDate;

    /**
     * @var PricingHelper
     */
    public $pricingHelper;

    /**
     * @var ProductRepositoryInterface
     */
    public $productRepository;

    /**
     * @var LinkRepositoryInterface
     */
    public $linkRepositoryInterface;

    /**
     * @var \Webkul\Marketplace\Model\ProductFactory
     */
    public $productFactory;

    /**
     * @var \Magento\Framework\App\Response\Http
     */
    public $response;

    /**
     * @var \Magento\Framework\App\Request\Http
     */
    public $request;

    /**
     * @var ConfigurableProTypeModel
     */
    public $configurableProTypeModel;

    /**
     * @var ProductCollectionFactory
     */
    public $productCollection;

    /**
     * \Magento\Framework\Module\Manager
     */
    public $moduleManager;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    public $storeManager;

    /**
     * @var \Magento\Framework\ObjectManagerInterface
     */
    public $objectManager;

    /**
     * @param \Magento\Framework\App\Helper\Context $context
     * @param TimezoneInterface                     $localeDate
     * @param ProductRepositoryInterface            $productRepository
     * @param LinkRepositoryInterface               $linkRepositoryInterface
     * @param PricingHelper                         $pricingHelper
     * @param LinkRepositoryInterface               $linkRepositoryInterface
     * @param ProductFactory                        $productFactory
     * @param \Magento\Framework\App\Response\Http  $response
     * @param ConfigurableProTypeModel              $configurableProTypeModel
     * @param \Magento\Framework\App\Request\Http   $request
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        TimezoneInterface $localeDate,
        ProductRepositoryInterface $productRepository,
        LinkRepositoryInterface $linkRepositoryInterface,
        PricingHelper $pricingHelper,
        ProductFactory $productFactory,
        \Magento\Framework\App\Response\Http $response,
        ConfigurableProTypeModel $configurableProTypeModel,
        \Magento\Framework\App\Request\Http $request,
        ProductCollectionFactory $productCollection,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\ObjectManagerInterface $objectManager,
        \Magento\Framework\App\Cache\TypeListInterface $cacheTypeList,
        \Magento\Framework\Pricing\PriceCurrencyInterface $priceCurrencyObject
    ) {
        $this->localeDate = $localeDate;
        $this->priceCurrencyObject = $priceCurrencyObject;
        $this->pricingHelper = $pricingHelper;
        $this->productFactory = $productFactory;
        $this->productRepository = $productRepository;
        $this->linkRepositoryInterface = $linkRepositoryInterface;
        $this->response = $response;
        $this->configurableProTypeModel = $configurableProTypeModel;
        $this->request = $request;
        $this->productCollection = $productCollection;
        $this->moduleManager = $context->getModuleManager();
        $this->storeManager = $storeManager;
        $this->objectManager = $objectManager;
        $this->cacheTypeList = $cacheTypeList;
        $this->today = $this->converToTz($this->localeDate->date()->format('Y-m-d H:i:s'));
        parent::__construct($context);
    }

    /**
     * @param object $product
     * @return array|flase []
     */
    public function getProductDealDetail($product)
    {
        $product = $this->productRepository->getById($product->getEntityId());
        $dealStatus = $product->getDealStatus();
        $content = false;
        $modEnable = $this->scopeConfig->getValue('mpdailydeals/general/enable');
        if ($modEnable) {
            $content = ['deal_status' => $dealStatus];
            $today = $this->localeDate->date()->format('Y-m-d H:i:s');
            $dealFromDateTime = $this->localeDate->date(
                strtotime($product->getDealFromDate())
            )->format('Y-m-d H:i:s');
            $dealToDateTime = $this->localeDate->date(
                strtotime($product->getDealToDate())
            )->format('Y-m-d H:i:s');
            $difference = strtotime($dealToDateTime) - strtotime($today);
            $specialPrice = $product->getSpecialPrice();
            $price = $product->getPrice();
            if ($modEnable && $difference > 0 && $dealFromDateTime < $today && $product->getDealStatus()) {
                $content['update_url'] = $this->_urlBuilder->getUrl('mpdailydeal/index/updatedealinfo');
                $content['stoptime'] = $product->getSpecialToDate();
                $content['diff_timestamp'] = $difference;
                $content['discount-percent'] = $product->getDealDiscountPercentage();
                $content['special-price'] = $specialPrice;
                $content['deal-from-date'] = $dealFromDateTime;
                $content['deal-to-date'] = $dealToDateTime;
                $content['deal_id'] = $product->getEntityId();
                if ($product->getTypeId() != 'bundle') {
                    $content['saved-amount'] = $this->pricingHelper
                                                ->currency($price - $specialPrice, true, false);
                }
                $this->setPriceAsDeal($product);
            } elseif ($modEnable && ($dealToDateTime <= $today)) {
                $token = 0;
                
                if ($product->getDealStatus()) {
                    $token = 1;
                }
                if ($token) {
                    $product->setSpecialToDate(date("m/d/Y", strtotime('-1 day')));
                    $product->setSpecialFromDate(date("m/d/Y", strtotime('-2 day')));
                    $product->setDealStatus(0);
                    $product->setSpecialPrice('');
                    $product->getResource()->saveAttribute($product, 'deal_to_date');
                    $product->getResource()->saveAttribute($product, 'deal_from_date');
                    $existingMediaGalleryEntries = $product->getMediaGalleryEntries();
                    $product->setMediaGalleryEntries($existingMediaGalleryEntries);
                    $product->save();
                    $config = $this->configurableProTypeModel
                                ->getParentIdsByChild($product->getEntityId());
                    if ($this->request->getFullActionName() == 'catalog_product_view' && !isset($config[0])) {
                        $this->response->setRedirect($product->getProductUrl());
                    }
                    $this->cacheFlush();
                }
                $content = false;
            }
        }
        return $content;
    }

    /**
     * setPriceAsDeal
     * @param ProductRepositoryInterface $product
     * @return void
     */
    public function setPriceAsDeal($product)
    {
        $proType = $product->getTypeId();
        if ($product->getDealDiscountType() == 'percent') {
            if ($proType != 'bundle') {
                $price = $product->getPrice() * ($product->getDealValue()/100);
            } else {
                $price = $product->getDealValue();
                $product->setPrice(null);
            }
            $discount = $product->getDealValue();
        } else {
            $price = $product->getDealValue();
            if ($product->getPrice()) {
                $discount = ($product->getDealValue()/$product->getPrice())*100;
            } else {
                $discount = 0;
            }
        }
        $token = 0;
        if ($product->getSpecialPrice() == $price) {
            $token = 1;
        }
        
        $product->setDealDiscountPercentage(round(100-$discount));
        if ($proType == 'downloadable') {
            $links = $this->linkRepositoryInterface->getLinksByProduct($product);
            $product->setDownloadableLinks($links);
        }
        if ($proType != 'bundle') {
            if (!$token) {
                $product->setDealStatus(1);
                $product->setSpecialFromDate($product->getDealFromDate());
                $product->setSpecialToDate($product->getDealToDate());
                $product->setSpecialPrice($price);
                $existingMediaGalleryEntries = $product->getMediaGalleryEntries();
                $product->setMediaGalleryEntries($existingMediaGalleryEntries);
                $product->save();
                $config = $this->configurableProTypeModel->getParentIdsByChild($product->getEntityId());
                if ($this->request->getFullActionName() == 'catalog_product_view' && !isset($config[0])) {
                    $this->response->setRedirect($product->getProductUrl());
                }
                $product->unsetDealStatus();
            }
        }
    }

    /**
     * @param object $sellerId
     * @return array
     */
    public function getSellerProductsIds($sellerId)
    {
        $proIds = false;
        $sellerColl = $this->productFactory->create()->getCollection()
                                ->addFieldToFilter('seller_id', ['eq' =>$sellerId]);
        if ($sellerColl->getSize()) {
            $proIds[] = 0;
            foreach ($sellerColl as $product) {
                $proIds[] = $product->getMageproductId();
            }
        }
        return $proIds;
    }

    /**
     * get deal enable value
     *
     * @return boolean
     */
    public function isDealEnable()
    {
        return $this->scopeConfig->getValue('mpdailydeals/general/enable');
    }

    /**
     * get all Deal product Ids
     *
     * @return Array
     */
    public function getDealProductIds()
    {
        $ids = [];
        if ($this->isDealEnable()) {
            $collection = $this->productCollection->create();
            $collection->addAttributeToFilter('deal_status', 1);
            if ($this->moduleManager->isEnabled('Webkul_MpHyperLocal')) {
                $helper = $this->objectManager->get('Webkul\MpHyperLocal\Helper\Data');
                $sellerIds = $helper->getNearestSellers();
                $allowedProList = $helper->getNearestProducts($sellerIds);
                $collection->addAttributeToFilter('entity_id', ['in' => $allowedProList]);
            }
            $collection->addAttributeToFilter('deal_from_date', ['lt'=>$this->today]);
            $collection->addAttributeToFilter('deal_to_date', ['gt'=>$this->today]);
            $ids = $collection->getColumnValues('entity_id');
            $ids = $this->getConfigurableProIds($ids, $collection);
        }
        return $ids;
    }
    
    /**
     * Get Configurable Product Ids
     *
     * @param array $ids
     * @param \Magento\Catalog\Model\Product\Collection $collection
     * @return array
     */
    public function getConfigurableProIds($ids, $collection)
    {
        $associatedProdIds =  $collection->addAttributeToFilter('visibility', ['eq' => 1])
                            ->getColumnValues('entity_id');
        foreach ($associatedProdIds as $id) {
            $details = $this->getConfigProId($id);
            $ids = array_merge($ids, $details);
        }
        return $ids;
    }

    /**
     * Convert amount based on currency
     *
     * @param integer $amount
     * @param integer $store
     * @param string $currency
     * @return int
     */
    public function getConvertedAmount($amount = 0, $store = null, $currency = null)
    {
        $currency = $this->storeManager->getStore()->getCurrentCurrency()->getCode();
        if ($store == null) {
            $store = $this->storeManager->getStore()->getStoreId();
        }
        $rate = $this->priceCurrencyObject
                ->convert($amount, $includeContainer = true, $precision = 2, $store, $currency);
        return $rate;
    }

    /**
     * Load Product
     *
     * @param int $id
     * @return \Magento\Catalog\Model\Product
     */
    public function loadProductById($id)
    {
        return $this->productRepository->getById($id);
    }

    /**
     * Ger Configurable Product Id
     *
     * @param int $id
     * @return int
     */
    private function getConfigProId($id)
    {
        return $this->configurableProTypeModel->getParentIdsByChild($id);
    }

    /**
     * Flush Cache
     */
    public function cacheFlush()
    {
        $types = ['full_page'];
        foreach ($types as $type) {
            $this->cacheTypeList->cleanType($type);
        }
    }

    public function getMaxDiscount($allDeals)
    {
        $minVal = 99999999;
        $result = [];
        foreach ($allDeals as $deal) {
            if ($deal['special-price']<=$minVal) {
                $minVal = $deal['special-price'];
                $result = $deal;
            }
        }
        return $result;
    }

    public function converToTz($dateTime = "", $fromTz = '', $toTz = '')
    {
        if (!$fromTz) {
            $fromTz = $this->localeDate->getConfigTimezone();
        }
        // timezone by php friendly values
        $date = new \DateTime($dateTime, new \DateTimeZone($fromTz));
        $date->setTimezone(new \DateTimeZone('UTC'));
        $dateTime = $date->format('Y-m-d H:i:s');
        return $dateTime;
    }
}

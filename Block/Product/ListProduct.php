<?php
namespace Webkul\MpDailyDeal\Block\Product;

/**
 * Webkul_DailyDeals ListProduct collection block.
 * @category  Webkul
 * @package   Webkul_DailyDeals
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory;
use Magento\Catalog\Api\CategoryRepositoryInterface;
use Magento\Reports\Model\ResourceModel\Product as ReportsProducts;
use Magento\Sales\Model\ResourceModel\Report\Bestsellers as SalesReportFactory;
use Magento\Catalog\Block\Product\ProductList\Toolbar;
use Magento\Catalog\Model\ResourceModel\Product\Collection as ProductCollection;

class ListProduct extends \Magento\Catalog\Block\Product\ListProduct
{
    /**
     * \Magento\Framework\Module\Manager
     */
    protected $_moduleManager;

    /**
     * Product Collection
     *
     * @var AbstractCollection
     */
    protected $_productCollection;

    /**
     * @param Context $context
     * @param \Magento\Framework\Data\Helper\PostHelper $postDataHelper
     * @param \Magento\Catalog\Model\Layer\Resolver     $layerResolver
     * @param CategoryRepositoryInterface               $categoryRepository
     * @param \Magento\Framework\Url\Helper\Data        $urlHelper
     * @param CollectionFactory                         $productFactory
     * @param ReportsProducts\CollectionFactory         $reportproductsFactory,
     * @param SalesReportFactory\CollectionFactory      $salesReportFactory
     */
    public function __construct(
        \Magento\Catalog\Block\Product\Context $context,
        \Magento\Framework\Data\Helper\PostHelper $postDataHelper,
        \Magento\Catalog\Model\Layer\Resolver $layerResolver,
        CategoryRepositoryInterface $categoryRepository,
        \Magento\Framework\Url\Helper\Data $urlHelper,
        CollectionFactory $productFactory,
        ReportsProducts\CollectionFactory $reportproductsFactory,
        \Magento\Framework\Module\Manager $moduleManager,
        \Magento\Framework\ObjectManagerInterface $objectManager,
        SalesReportFactory\CollectionFactory $salesReportFactory,
        \Magento\ConfigurableProduct\Model\Product\Type\Configurable $configurable
    ) {
        $this->_productFactory = $productFactory;
        $this->_reportproductsFactory = $reportproductsFactory;
        $this->_salesReportFactory = $salesReportFactory;
        $this->_moduleManager = $moduleManager;
        $this->_objectManager= $objectManager;
        $this->configurable = $configurable;
        parent::__construct(
            $context,
            $postDataHelper,
            $layerResolver,
            $categoryRepository,
            $urlHelper
        );
        $this->_today = $this->converToTz($this->_localeDate->date()->format('Y-m-d H:i:s'));
    }

    /**
     * @return bool|\Magento\Catalog\Model\ResourceModel\Product\Collection
     */
    public function _getProductCollection()
    {
        if (!$this->_productCollection) {
            $paramData = $this->getRequest()->getParams();
            $productname = $this->getRequest()->getParam('name');
            $simpledealIds = $this->_productFactory
                ->create()
                ->addAttributeToSelect('*')
                ->addAttributeToFilter('deal_status', 1)
                ->addFieldToFilter('visibility', ['neq' => 1])
                ->addAttributeToFilter(
                    'deal_from_date',
                    ['lt'=>$this->_today]
                )->addAttributeToFilter(
                    'deal_to_date',
                    ['gt'=>$this->_today]
                )->getColumnValues('entity_id');
            $notvisibleIds = $this->_productFactory
                ->create()
                ->addAttributeToSelect('*')
                ->addAttributeToFilter('deal_status', 1)
                ->addFieldToFilter('visibility', ['eq' => 1])
                ->addAttributeToFilter(
                    'deal_from_date',
                    ['lt'=>$this->_today]
                )->addAttributeToFilter(
                    'deal_to_date',
                    ['gt'=>$this->_today]
                )->getColumnValues('entity_id');
            $configurableIds = [0];
            foreach ($notvisibleIds as $notvisibleId) {
                $parentIds = $this->configurable->getParentIdsByChild($notvisibleId);
                $configurableIds = array_merge($configurableIds, $parentIds);
            }
            $allProductIds = array_merge($simpledealIds, $configurableIds);
            
            $collection = $this->_productFactory
                            ->create()
                            ->addAttributeToSelect('*')
                            ->addFieldToFilter('entity_id', ['in'=>$allProductIds]);

            $layer = $this->getLayer();

            $origCategory = null;
    
            $this->prepareSortableFieldsByCategory($layer->getCurrentCategory());

            $this->_productCollection = $collection;

            if ($origCategory) {
                $layer->setCurrentCategory($origCategory);
            }
            $toolbar = $this->getToolbarBlock();
            $this->configureProductToolbar($toolbar, $collection);
        }
        $this->_productCollection->getSize();

        return $this->_productCollection;
    }

    /**
     * Configures the Toolbar block for sorting related data.
     *
     * @param ProductList\Toolbar $toolbar
     * @param ProductCollection $collection
     * @return void
     */
    public function configureProductToolbar(Toolbar $toolbar, ProductCollection $collection)
    {
        $availableOrders = $this->getAvailableOrders();
        if (isset($availableOrders['position'])) {
            unset($availableOrders['position']);
        }
        if ($availableOrders) {
            $toolbar->setAvailableOrders($availableOrders);
        }
        $sortBy = $this->getSortBy();
        if ($sortBy) {
            $toolbar->setDefaultOrder($sortBy);
        }
        $defaultDirection = $this->getDefaultDirection();
        if ($defaultDirection) {
            $toolbar->setDefaultDirection($defaultDirection);
        }
        $sortModes = $this->getModes();
        if ($sortModes) {
            $toolbar->setModes($sortModes);
        }
        // set collection to toolbar and apply sort
        $toolbar->setCollection($collection);
        $this->setChild('toolbar', $toolbar);
    }

    public function getDefaultDirection()
    {
        return 'asc';
    }

    public function getSortBy()
    {
        return 'name';
    }

    /**
     * getTopDealsOfDay
     * @return CollectionFactory top 5 products on best deal
     */
    public function getTopDealsOfDay()
    {
        return $this->_productFactory
                        ->create()
                        ->addAttributeToSelect('*')
                        ->addAttributeToFilter('deal_status', 1)
                        ->addAttributeToFilter(
                            'deal_from_date',
                            ['lt'=>$this->_today]
                        )->addAttributeToFilter(
                            'deal_to_date',
                            ['gt'=>$this->_today]
                        )->setOrder(
                            'deal_discount_percentage',
                            'DESC'
                        )
                        ->addFieldToFilter('status', 1)
                        ->addFieldToFilter('visibility', ['neq' => 1])
                        ->setPageSize(5);
    }

    /**
     * getDealProductImage
     * @param Magento\Catalog\Model\Product $product
     * @return string product image url
     */
    public function getDealProductImage($product)
    {
        return $this->_imageHelper
                        ->init($product, 'category_page_grid')
                            ->constrainOnly(false)
                            ->keepAspectRatio(true)
                            ->keepFrame(false)
                            ->resize(400)
                            ->getUrl();
    }

    /**
     * getTopDealViewsProduct
     * @return ReportsProducts // top 5 viewed product
     */
    public function getTopDealViewsProduct()
    {
        return $this->_reportproductsFactory
                        ->create()
                        ->addAttributeToSelect('*')
                        ->addViewsCount()
                        ->setStoreId(0)
                        ->addStoreFilter(0)
                        ->addAttributeToFilter('deal_status', 1)
                        ->addFieldToFilter('status', 1)
                        ->addFieldToFilter('visibility', ['neq' => 1])
                        ->addAttributeToFilter(
                            'deal_from_date',
                            ['lt'=>$this->_today]
                        )->addAttributeToFilter(
                            'deal_to_date',
                            ['gt'=>$this->_today]
                        )->setPageSize(5);
    }

    /**
     * getDealViewsProduct
     * @return SalesReportFactory //best sold product
     */
    public function getTopSaleProduct()
    {
        $productIds = $this->_salesReportFactory->create()->setModel('Magento\Catalog\Model\Product')
                                            ->addStoreFilter(0)->setPageSize(5)
                                            ->getColumnValues('product_id');

        if (empty($productIds)) {
            $productIds = [0];
        }
        return $this->_reportproductsFactory
                        ->create()
                        ->addAttributeToSelect('*')
                        ->addFieldToFilter('entity_id', ['in' => $productIds])
                        ->addFieldToFilter('visibility', ['neq' => 1])
                        ->addFieldToFilter('status', 1)
                        ->setPageSize(5);
    }
    public function converToTz($dateTime = "", $fromTz = '', $toTz = '')
    {
        if (!$fromTz) {
            $fromTz = $this->_localeDate->getConfigTimezone();
        }
        // timezone by php friendly values
        $date = new \DateTime($dateTime, new \DateTimeZone($fromTz));
        $date->setTimezone(new \DateTimeZone('UTC'));
        $dateTime = $date->format('Y-m-d H:i:s');
        return $dateTime;
    }
}

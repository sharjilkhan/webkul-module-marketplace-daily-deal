<?php
namespace Webkul\MpDailyDeal\Block\Plugin;

/**
 * Webkul MpDailyDeal ProductListUpdateForDeals plugin.
 * @category  Webkul
 * @package   Webkul_MpDailyDeals
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

use Magento\Catalog\Block\Product\ListProduct;
use Magento\Catalog\Model\Product;
use Magento\ConfigurableProduct\Model\Product\Type\Configurable as ConfigurableProTypeModel;

class ProductListUpdateForDeals
{
    /**
     * @var \Webkul\MpDailyDeals\Helper\Data
     */
    public $mpDailyDealHelper;

    /**
     * @param Webkul\MpDailyDeals\Helper\Data $dailyDealHelper
     */
    public function __construct(
        \Webkul\MpDailyDeal\Helper\Data $mpDailyDealHelper,
        ConfigurableProTypeModel $configurableProTypeModel,
        Product $product
    ) {
        $this->_configurableProTypeModel = $configurableProTypeModel;
        $this->_product = $product;
        $this->mpDailyDealHelper = $mpDailyDealHelper;
    }
 
    /**
     * beforeGetProductPrice // update deal details before price render
     * @param ListProduct                    $list
     * @param \Magento\Catalog\Model\Product $product
     */
    public function beforeGetProductPrice(
        ListProduct $list,
        $product
    ) {
        $dealDetail = $this->mpDailyDealHelper->getProductDealDetail($product);
    }

    /**
     * aroundGetProductPrice // add clock data html product price
     * @param ListProduct                    $list
     * @param Object                         $proceed
     * @param \Magento\Catalog\Model\Product $product
     */
    public function aroundGetProductPrice(
        ListProduct $list,
        $proceed,
        $product
    ) {
        $dealDetailHtml = "";
        $type = $product->getTypeId();
        if ($type != 'configurable') {
            $dealDetail = $this->mpDailyDealHelper->getProductDealDetail($product);
            if ($dealDetail && $dealDetail['deal_status'] && isset($dealDetail['diff_timestamp'])) {
                $dealDetailHtml = '<div class="deal wk-daily-deal" data-deal-id="'.$product->getId()
                    .'" data-update-url="'.$dealDetail['update_url'].'">
                    <div class="wk-deal-off-box"><div class="wk-deal-left-border">
                    </div><span class="deal-price-label">'
                    .$dealDetail['discount-percent'].'% '.__('Off')
                    .'</span></div><span class="price-box ">';
                $saveBox = isset($dealDetail['saved-amount'])? '<p class="wk-save-box "><span class="save-label">'
                    . __('Save On Deal ').'<span class="wk-deal-font-weight-600">'.$dealDetail['saved-amount']
                    .'</span></span></p>' : '' ;

                $saveBoxAvl = isset($dealDetail['saved-amount']) ? '' : 'notavilable';
                $dealDetailHtml = $dealDetailHtml.'<span class="wk-deal-ends-label">'
                    .__('Deal Ends in ').'&nbsp</span><p class="wk_cat_count_clock wk-deal-font-weight-600'
                    .$saveBoxAvl.'" data-stoptime="'
                .$dealDetail['stoptime'].'" data-diff-timestamp ="'.$dealDetail['diff_timestamp']
                .'"></p>'.$saveBox.'</div>';
            }
            if (!$product->getSpecialPrice() && isset($dealDetail['special-price'])) {
                $product->setSpecialPrice($dealDetail['special-price']);
                $product->setSpecialFromDate($dealDetail['deal-from-date']);
                $product->setSpecialToDate($dealDetail['deal-to-date']);
            }
        } elseif ($type == 'configurable') {
            $alldeal = [];
            $alldealDetails = [];
            $associatedProducts = $this->getAllAssociatedProducts($product->getId());
            foreach ($associatedProducts as $key => $value) {
                $dealDetail = $this->getChildProductDealDetail($value);
                if ($dealDetail && $dealDetail['deal_status']
                && isset($dealDetail['diff_timestamp'])) {
                    $alldeal[$value] = $dealDetail['saved-amount'];
                    $alldealDetails[$value] = $dealDetail;
                }
            }
            $dealDetail = [];
            arsort($alldeal);
            $i = 1;
            foreach ($alldeal as $key => $value) {
                if ($i == 1) {
                    $dealDetail = $alldealDetails[$key];
                }
            }
            if ($dealDetail) {
                $dealDetailHtml = '<div class="deal wk-daily-deal" data-deal-id="'.$product->getId()
                                    .'" data-update-url="'
                                    .$dealDetail['update_url'].'"><div class="wk-deal-off-box">
                                    <div class="wk-deal-left-border"></div><span class="deal-price-label">
                                    '.$dealDetail['discount-percent'].'% '.__('Off')
                                    .'</span></div><span class="price-box ">';
                $saveBox = isset($dealDetail['saved-amount'])? '<p class="wk-save-box "><span class="save-label">'
                    . __('Max Save On Deal ').'<span class="wk-deal-font-weight-600">'
                    .$dealDetail['saved-amount'].'</span></span></p>' : '' ;

                $saveBoxAvl = isset($dealDetail['saved-amount']) ? '' : 'notavilable';
                $dealDetailHtml = $dealDetailHtml.'<span class="wk-deal-ends-label">'
                    .__('Deal Ends in ').'&nbsp</span><p class="wk_cat_count_clock wk-deal-font-weight-600'
                    .$saveBoxAvl.'" data-stoptime="'
                    .$dealDetail['stoptime'].'" data-diff-timestamp ="'.$dealDetail['diff_timestamp']
                    .'"></p>'.$saveBox.'</div>';
            }
            $product = $this->setDetails($product, $dealDetail);
        }
        return $proceed($product).$dealDetailHtml;
    }

    /**
     * Get Associated product details
     *
     * @param int $proId
     * @return array
     */
    public function getChildProductDealDetail($proId)
    {
        $product = $this->_product->load($proId);
        return $this->mpDailyDealHelper->getProductDealDetail($product);
    }

    /**
     * Get Associated product
     *
     * @param int $id
     * @return array
     */
    public function getAllAssociatedProducts($id)
    {
        $childProductsIds = $this->_configurableProTypeModel->getChildrenIds($id);
        return $childProductsIds[0];
    }

    /**
     * Set Special Price
     *
     * @param \Magento\Catalog\Model\Product $product
     * @param array $dealDetail
     * @return \Magento\Catalog\Model\Product
     */
    private function setDetails($product, $dealDetail)
    {
        if (!$product->getSpecialPrice() && isset($dealDetail['special-price'])) {
            $product->setSpecialPrice($dealDetail['special-price']);
            $product->setSpecialFromDate($dealDetail['deal-from-date']);
            $product->setSpecialToDate($dealDetail['deal-to-date']);
        }
        return $product;
    }
}

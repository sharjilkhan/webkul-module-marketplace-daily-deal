<?php
/**
 * Webkul_MpDailyDeal Collection controller.
 * @category  Webkul
 * @package   Webkul_MpDailyDeal
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\MpDailyDeal\Controller\Index;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use Magento\Catalog\Api\CategoryRepositoryInterface;

class GetTimes extends Action
{
    /**
     * @var PageFactory
     */
    private $resultPageFactory;

    /**
     * @param Context     $context
     * @param PageFactory $resultPageFactory
     */
    public function __construct(
        Context $context,
        \Magento\Framework\Json\Helper\Data $jsonData,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,
        \Magento\Catalog\Model\ProductFactory $productFactory,
        \Magento\Framework\Stdlib\DateTime\TimezoneInterface $timezone,
        \Magento\ConfigurableProduct\Model\Product\Type\Configurable $configurable,
        \Webkul\MpDailyDeal\Helper\Data $helper
    ) {
        $this->jsonData = $jsonData;
        $this->helper = $helper;
        $this->productCollectionFactory = $productCollectionFactory;
        $this->productFactory = $productFactory;
        $this->configurable = $configurable;
        $this->today = $timezone->convertConfigTimeToUtc($timezone->date());
        parent::__construct($context);
    }

    /**
     * MpDailyDeal Product Collection Page.
     *
     * @return \Magento\Framework\View\Result\Page
     */
    public function execute()
    {
        try {
            $collection = $this->getProductCollection();
            $result = [];
            foreach ($collection as $value) {
                $dealDetail  = $this->getCurrentProductDealDetail($value);
                $dealDetail['parent'] = $this->configurable->getParentIdsByChild($value->getId());
                $result[$value->getId()] = $dealDetail;
            }
            $this->getResponse()->setHeader('Content-type', 'application/javascript');
            $this->getResponse()->setBody($this->jsonData
                ->jsonEncode(
                    [
                        'success' => 1,
                        'data' => $result
                    ]
                ));
        } catch (\Exception $e) {
            $this->getResponse()->setHeader('Content-type', 'application/javascript');
            $this->getResponse()->setBody($this->jsonData
                ->jsonEncode(
                    [
                        'success' => 0,
                        'message' => __('Something went wrong in getting spin wheel.')
                    ]
                ));
        }
    }

    /**
     * @return Magento\Eav\Model\Entity\Collection\AbstractCollection
     */
    protected function getProductCollection()
    {
        $simpledealIds = $this->productCollectionFactory
                                    ->create()
                                    ->addAttributeToSelect('*')
                                    ->addAttributeToFilter('deal_status', 1)
                                    ->addFieldToFilter('visibility', ['neq' => 1])
                                    ->addAttributeToFilter(
                                        'deal_from_date',
                                        ['lt'=>$this->today]
                                    )->addAttributeToFilter(
                                        'deal_to_date',
                                        ['gt'=>$this->today]
                                    )->getColumnValues('entity_id');
        $notvisibleIds = $this->productCollectionFactory
                                    ->create()
                                    ->addAttributeToSelect('*')
                                    ->addAttributeToFilter('deal_status', 1)
                                    ->addFieldToFilter('visibility', ['eq' => 1])
                                    ->addAttributeToFilter(
                                        'deal_from_date',
                                        ['lt'=>$this->today]
                                    )->addAttributeToFilter(
                                        'deal_to_date',
                                        ['gt'=>$this->today]
                                    )->getColumnValues('entity_id');
        $configurableIds = [0];
        foreach ($notvisibleIds as $notvisibleId) {
            $parentIds = $this->configurable->getParentIdsByChild($notvisibleId);
            $configurableIds = array_merge($configurableIds, $parentIds);
        }
        $allProductIds = array_merge($simpledealIds, $configurableIds);
        if (!empty($notvisibleIds)) {
            $allProductIds = array_merge($allProductIds, $notvisibleIds);
        }
        $collection = $this->productCollectionFactory
                                ->create()
                                ->addAttributeToSelect('*')
                                ->addFieldToFilter('entity_id', ['in'=>$allProductIds]);
        return $collection;
    }

        /**
         * @return array Product Deal Detail
         */

    public function getCurrentProductDealDetail($curPro)
    {
        $productType = $curPro->getTypeId();
        $assDealDetails = [];
        if ($productType == "configurable") {
            $dataDeal = $this->getConfigAssociateProDeals($curPro);
        } else {
            $dataDeal = $this->helper->getProductDealDetail($curPro);
            if ($dataDeal) {
                $dataDeal['entity_id'] = $curPro->getId();
            }
        }
        return $dataDeal;
    }

    /**
     * getConfigAssociateProDeals
     * @param Magento\Catalog\Model\Product $curPro
     * @return boolen|array
     */

    public function getConfigAssociateProDeals($curPro)
    {
        $configProId = $curPro->getId();
        $alldeal = [];
        $associatedProducts = $this->configurable->getChildrenIds($configProId);
        foreach ($associatedProducts[0] as $assProId) {
            $dealDetail = $this->getChildProductDealDetail($assProId);
            if ($dealDetail
                && isset($dealDetail['deal_status'])
                && $dealDetail['deal_status']
                && isset($dealDetail['diff_timestamp'])
            ) {
                $alldeal[$assProId] = $dealDetail['saved-amount'];
                $dealDetail['entity_id'] = $assProId;
                $dealDetail['pro_type'] = 'configurable';
                $assDealDetails[$assProId] = $dealDetail;
            }
        }
        if (isset($assDealDetails)) {
            $dealDetail = $this->helper->getMaxDiscount($assDealDetails);
            if (!empty($dealDetail)) {
                return $dealDetail;
            }
        }
        return false;
    }

    /**
     * getChildProductDealDetail
     * @param int $proId
     * @return Magento\Catalog\Model\Product
     */
    public function getChildProductDealDetail($proId)
    {
        $product = $this->productFactory->create()->load($proId);
        $dealvalue = $product->getDealValue();
        if ($product->getDealDiscountType() == 'percent') {
            $price = $product->getPrice() * ($dealvalue/100);
            $discount = $dealvalue;
        } else {
            $price = $dealvalue;
            $discount = ($dealvalue/$product->getPrice())*100;
        }
        $dealData = $this->helper->getProductDealDetail($product);
        if (!$dealData['discount-percent']) {
            $dealData['discount-percent'] = round(100-$discount);
        }
        return $dealData;
    }
}
